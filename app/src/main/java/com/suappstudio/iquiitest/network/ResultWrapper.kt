package com.indra.lyc.network

import com.suappstudio.iquiitest.model.ErrorResponse


sealed class ResultWrapper<out T> {
    data class Success<out T>(val data: T?) : ResultWrapper<T>()
    data class GenericError(val code : Int? = null, val error: String?) : ResultWrapper<Nothing>()
    object NetworkError: ResultWrapper<Nothing>()
}