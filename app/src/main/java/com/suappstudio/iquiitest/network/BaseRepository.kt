package com.indra.lyc.network


import retrofit2.HttpException
import java.io.IOException

open class BaseRepository {

    suspend fun <T> safeApiCall(apiCall: suspend () -> T): ResultWrapper<T> {
            return try {
                val data = apiCall.invoke()

                ResultWrapper.Success(data)
            } catch (throwable: Throwable) {
                when (throwable) {
                    is IOException ->
                        return ResultWrapper.NetworkError
                    is HttpException -> {
                        val code = throwable.code()
                        val errorResponse = convertErrorBody(throwable)
                        ResultWrapper.GenericError(code, errorResponse)
                    }
                    else -> {
                         ResultWrapper.GenericError(null, null)
                    }
                }
            }

    }

    /*private suspend fun <T: Any> safeApiResult(call: suspend ()-> Response<T>, errorMessage: String) : ResultWrapper<T> {
        val response = call.invoke()
        if(response.isSuccessful) return ResultWrapper.Success(response.body()!!)
            return ResultWrapper.GenericError(response.errorBody(!))
    }*/

    private fun convertErrorBody(throwable: HttpException): String? {
        return try {

            throwable.response()?.errorBody()?.string()
        } catch (exception: Exception) {
            null
        }
    }
}