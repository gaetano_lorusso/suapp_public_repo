package com.suappstudio.iquiitest.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.viewModelScope
import com.suappstudio.iquiitest.AppDatabase
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class PagerViewModel(val app : Application) : AndroidViewModel(app) {

    fun setImage(image: RedditImage){
        viewModelScope.launch(Dispatchers.IO) {
            AppDatabase.getInstance(app).imagesDao.setImage(image)
        }

    }
}