package com.suappstudio.iquiitest.ui

import android.os.Bundle

import android.view.*
import android.view.inputmethod.EditorInfo
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.SharedElementCallback
import androidx.core.view.isVisible
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.FragmentNavigatorExtras
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.transition.TransitionInflater
import androidx.transition.TransitionSet
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class ListFragment : Fragment() {

    val vModel : ListViewModel by viewModel()
    val data = ArrayList<RedditImage>()
    lateinit var adapter : ImagesGridAdapter
    var position : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        setHasOptionsMenu(true)

        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.app_name)
        adapter = ImagesGridAdapter(data){
                index, view ->
            val name = data[index].title
            (exitTransition as TransitionSet).excludeTarget(view,true)
            val extras = FragmentNavigatorExtras(Pair(view,name))
            val action = ListFragmentDirections.actionListFragmentToFragmentPager(data.toTypedArray(),index)
            findNavController().navigate(action,extras)
        }
        vModel.imagesLive.observe(viewLifecycleOwner){
            progress.isVisible = false
            data.clear()
            if(it.size >0){
                data.addAll(it)
                empty.isVisible = false
            }
            else{
                empty.isVisible = true
            }

            adapter.notifyDataSetChanged()
        }
        return inflater.inflate(R.layout.list_fragment,container,false)
    }

    private fun prepareTransitions() {
        exitTransition = TransitionInflater.from(requireContext()).inflateTransition(R.transition.grid_exit_transition)
        setExitSharedElementCallback(object : SharedElementCallback(){
            override fun onMapSharedElements(
                names: MutableList<String>,
                sharedElements: MutableMap<String, View>
            ) {

               val selected = picture_list.findViewHolderForAdapterPosition(position)
                if(selected == null){
                    return
                }
                sharedElements.put(names.get(0), selected.itemView.findViewById(R.id.image));
            }
        })

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        input_word.doAfterTextChanged {
            if(it != null && it.length > 2){
                progress.isVisible = true
                vModel.getRedditImages(it?.toString())
            }
        }

        input_word.setOnEditorActionListener(){v, actionId, event ->
            if(input_word.text!!.length > 2 && actionId == EditorInfo.IME_ACTION_SEARCH){
                progress.isVisible = true
                vModel.getRedditImages(input_word.text!!.toString())
                true
            } else {
                false
            }

        }


        picture_list.layoutManager = GridLayoutManager(requireContext(),3)
        picture_list.addItemDecoration(VerticalSpaceItemDecoration(24))
        picture_list.adapter = adapter




        vModel.errorLive.observe(viewLifecycleOwner){
            progress.isVisible = false
            AlertDialog.Builder(requireContext())
                .setMessage(it)
                .show()
        }

        prepareTransitions()
    }



    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.main_menu, menu);
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean = when (item.itemId) {
        R.id.prefs ->{
            val destination = findNavController().currentDestination!!.id
            if(destination != R.id.prefFragment){
                findNavController().navigate(R.id.prefFragment)
            }
            true
        }
        else ->{
            super.onOptionsItemSelected(item)
        }
    }
}