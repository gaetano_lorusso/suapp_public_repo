package com.suappstudio.iquiitest.ui

import android.os.Bundle
import android.view.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.NavArgs
import androidx.navigation.fragment.navArgs
import coil.Coil
import coil.api.load
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.android.synthetic.main.image_large.*
import kotlinx.android.synthetic.main.image_pager.*
import kotlin.math.max
import kotlin.math.min

const val IMAGE = "image"


class ImageLarge : Fragment(),View.OnTouchListener {

   // val args : ImageLargeArgs by navArgs()

    companion object{
        fun getInstance(image : RedditImage) : ImageLarge{
            val fragment = ImageLarge()
            fragment.arguments = bundleOf(Pair(IMAGE,image))
            return fragment
        }
    }
    private lateinit var scaleGestureDetector: ScaleGestureDetector
    private var scaleFactor = 1.0f

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val root = inflater.inflate(R.layout.image_large,container,false)
        root.setOnTouchListener(this)
        return root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val item = requireArguments().getParcelable<RedditImage>(IMAGE)

        val title = item?.title
        image.transitionName = title
        Coil.load(requireContext(),item!!.url){
            target{

                image.setImageDrawable(it)
                requireParentFragment().startPostponedEnterTransition()
            }
            placeholder(R.drawable.loading)
            error(R.drawable.ic_baseline_broken_image_24)
        }


        (activity as AppCompatActivity).supportActionBar?.title = title
        scaleGestureDetector = ScaleGestureDetector(requireContext(), ScaleListener())

    }


     inner class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener() {
        override fun onScale(scaleGestureDetector: ScaleGestureDetector): Boolean {
            scaleFactor *= scaleGestureDetector.scaleFactor
            scaleFactor = max(0.1f, min(scaleFactor, 10.0f))
            image.scaleX = scaleFactor
            image.scaleY = scaleFactor
            return true
        }
    }

    override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
        scaleGestureDetector.onTouchEvent(p1)
        return true
    }
}



