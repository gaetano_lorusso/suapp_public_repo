package com.suappstudio.iquiitest.ui

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentStatePagerAdapter
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.suappstudio.iquiitest.model.RedditImage

class ImagePagerAdapter(fragment: Fragment, val list : List<RedditImage>) : FragmentStateAdapter(fragment) {


    override fun getItemCount(): Int {
        return list.size
    }

    override fun createFragment(position: Int): Fragment {
       return ImageLarge.getInstance(list[position])
    }
}