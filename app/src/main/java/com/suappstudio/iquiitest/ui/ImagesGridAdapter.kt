package com.suappstudio.iquiitest.ui

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.android.synthetic.main.image_item.view.*

class ImagesGridAdapter(val items : List<RedditImage>, val onItemClick :((index : Int, view : View) -> Unit)) : RecyclerView.Adapter<ImagesGridAdapter.ImageHolder>(){

     inner class ImageHolder(itemView: View) : RecyclerView.ViewHolder(itemView){

        fun bind(item : RedditImage){

            itemView.image.load(item.url){
                placeholder(R.drawable.loading)
                error(R.drawable.ic_baseline_broken_image_24)
            }
            itemView.setOnClickListener {
                onItemClick.invoke(adapterPosition,itemView.image)
            }
            itemView.image.transitionName = item.title
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        return ImageHolder(layoutInflater.inflate(R.layout.image_item,parent,false))
    }

    override fun onBindViewHolder(holder: ImageHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount(): Int {
       return items.size
    }
}