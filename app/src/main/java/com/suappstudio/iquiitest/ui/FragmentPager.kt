package com.suappstudio.iquiitest.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.app.SharedElementCallback
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.transition.Transition
import androidx.transition.TransitionInflater
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.findCurrentFragment
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.android.synthetic.main.image_pager.*
import org.koin.android.viewmodel.ext.android.viewModel


class FragmentPager : Fragment() {

    val args : FragmentPagerArgs by navArgs()
    val vModel : PagerViewModel by viewModel()
    lateinit var images : Array<RedditImage>
     var index : Int = 0

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       images = args.images
        index = args.index

        prepareSharedElementTransition()
// Avoid a postponeEnterTransition on orientation change, and postpone only of first creation.
        if (savedInstanceState == null) {
            postponeEnterTransition();
        }
        return inflater.inflate(R.layout.image_pager, container, false)
    }

    private fun prepareSharedElementTransition() {
        val transition: Transition = TransitionInflater.from(context)
            .inflateTransition(R.transition.image_shared_element_transition)
        sharedElementEnterTransition = transition
        setEnterSharedElementCallback(object : SharedElementCallback() {
            override fun onMapSharedElements(
                names: MutableList<String>,
                sharedElements: MutableMap<String, View>
            ) {
                val currentFragment = viewpager.findCurrentFragment(childFragmentManager)
                val view = currentFragment!!.view ?: return
                sharedElements.put(names.get(0), view.findViewById(R.id.image));
            }
        })
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        viewpager.adapter = ImagePagerAdapter(this, images.toList())
        viewpager.setCurrentItem(index, false)
        save_button.setOnClickListener {
            val image = images[viewpager.currentItem]
            vModel.setImage(image)
        }
        save_button.isVisible = !args.fromPref
    }


}