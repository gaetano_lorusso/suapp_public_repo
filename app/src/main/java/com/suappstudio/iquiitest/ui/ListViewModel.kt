package com.suappstudio.iquiitest.ui

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.indra.lyc.network.ResultWrapper
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.model.RedditImage
import com.suappstudio.iquiitest.repo.RedditeRepo
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class ListViewModel(val app : Application,val  repository : RedditeRepo) : AndroidViewModel(app) {

    val imagesLive = MutableLiveData<ArrayList<RedditImage>>()
    val errorLive = MutableLiveData<String>()


    fun getRedditImages(word : String){

        viewModelScope.launch(Dispatchers.IO) {
            val list = ArrayList<RedditImage>()
            val response = repository.getImages(word)
            when(response){
                is ResultWrapper.Success ->{
                    val data = response.data
                    if(data!!.isSuccessful){
                        val items = data.body()!!.data.children
                        for(item in items){
                            val redditImage = RedditImage(item.data.url,item.data.title,false)
                            list.add(redditImage)
                        }
                        imagesLive.postValue(list)
                    }
                    else{
                        errorLive.postValue(app.getString(R.string.generic_error))
                    }
                }
                is ResultWrapper.NetworkError ->{
                    errorLive.postValue(app.getString(R.string.network_error))
                }
                is ResultWrapper.GenericError -> {
                    errorLive.postValue(app.getString(R.string.generic_error))
                }
            }

        }
    }

}