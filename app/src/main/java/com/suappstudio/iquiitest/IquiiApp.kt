package com.suappstudio.iquiitest

import android.app.Application
import com.suappstudio.iquiitest.di.list_module
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class IquiiApp : Application() {

    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@IquiiApp)
            modules(listOf(list_module))
        }

    }
}