package com.suappstudio.iquiitest

import android.app.Notification
import androidx.room.*
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.coroutines.flow.Flow

@Dao
interface ImagesDao {
    @Query("SELECT * FROM redditimage")
    fun getImagesFlow() : Flow<List<RedditImage>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun setImage(redditImage: RedditImage)

    @Update
    fun updateImage(redditImage: RedditImage)

    @Delete
    fun deleteImage(redditImage: RedditImage)
}