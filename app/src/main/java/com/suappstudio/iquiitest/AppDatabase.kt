package com.suappstudio.iquiitest

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.suappstudio.iquiitest.model.RedditImage

@Database(entities = [RedditImage::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase() {

    abstract val imagesDao : ImagesDao

    companion object {

        // For Singleton instantiation
        @Volatile private var instance: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase {
            return instance ?: synchronized(this) {
                instance
                    ?: buildDatabase(
                        context
                    ).also { instance = it }
            }
        }

        private fun buildDatabase(context: Context): AppDatabase {
            return Room.databaseBuilder(context, AppDatabase::class.java, DATABASE_NAME)
                .build()
        }


    }




}

private const val DATABASE_NAME = "images-db"

