package com.suappstudio.iquiitest.di

import com.suappstudio.iquiitest.pref.PrefRepo
import com.suappstudio.iquiitest.pref.PrefViewModel
import com.suappstudio.iquiitest.ui.ListViewModel
import com.suappstudio.iquiitest.repo.RedditeRepo
import com.suappstudio.iquiitest.ui.PagerViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val list_module = module {
    single { RedditeRepo() }
    viewModel { ListViewModel(get(),get()) }
    viewModel { PagerViewModel(get()) }
    viewModel { PrefViewModel(get(),get()) }
    single { PrefRepo(get()) }
}
