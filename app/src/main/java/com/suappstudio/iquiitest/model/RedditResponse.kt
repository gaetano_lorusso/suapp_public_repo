package com.suappstudio.iquiitest.model

data class RedditResponse(
    val `data`: Data,
    val kind: String
)