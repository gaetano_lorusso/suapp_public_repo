package com.suappstudio.iquiitest.model

data class Obfuscated(
    val resolutions: List<Resolution>,
    val source: Source
)