package com.suappstudio.iquiitest.model

data class Preview(
    val enabled: Boolean,
    val images: List<Image>
)