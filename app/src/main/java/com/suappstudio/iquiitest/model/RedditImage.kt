package com.suappstudio.iquiitest.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class RedditImage(@PrimaryKey val url : String, val title : String, val isPreferred : Boolean) : Parcelable