package com.suappstudio.iquiitest.model

data class ResizedStaticIcon(
    val height: Int,
    val url: String,
    val width: Int
)