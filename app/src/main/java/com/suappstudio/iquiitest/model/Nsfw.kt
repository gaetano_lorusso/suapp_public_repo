package com.suappstudio.iquiitest.model

data class Nsfw(
    val resolutions: List<Resolution>,
    val source: Source
)