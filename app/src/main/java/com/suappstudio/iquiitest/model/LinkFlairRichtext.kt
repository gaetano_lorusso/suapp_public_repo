package com.suappstudio.iquiitest.model

data class LinkFlairRichtext(
    val e: String,
    val t: String
)