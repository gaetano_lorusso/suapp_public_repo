package com.suappstudio.iquiitest.model

data class ResizedIcon(
    val height: Int,
    val url: String,
    val width: Int
)