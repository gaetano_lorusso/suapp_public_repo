package com.suappstudio.iquiitest.model

data class MediaEmbed(
    val content: String,
    val height: Int,
    val scrolling: Boolean,
    val width: Int
)