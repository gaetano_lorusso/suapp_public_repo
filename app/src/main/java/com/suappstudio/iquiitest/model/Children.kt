package com.suappstudio.iquiitest.model

data class Children(
    val `data`: DataX,
    val kind: String
)