package com.suappstudio.iquiitest.model

data class Resolution(
    val height: Int,
    val url: String,
    val width: Int
)