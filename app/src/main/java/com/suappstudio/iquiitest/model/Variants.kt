package com.suappstudio.iquiitest.model

data class Variants(
    val nsfw: Nsfw,
    val obfuscated: Obfuscated
)