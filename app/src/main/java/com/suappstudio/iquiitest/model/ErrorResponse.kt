package com.suappstudio.iquiitest.model

data class ErrorResponse(val code : Int, val problem: String? = null)