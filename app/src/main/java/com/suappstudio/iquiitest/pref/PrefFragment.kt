package com.suappstudio.iquiitest.pref

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import com.suappstudio.iquiitest.R
import com.suappstudio.iquiitest.model.RedditImage
import com.suappstudio.iquiitest.ui.ImagesGridAdapter
import com.suappstudio.iquiitest.ui.VerticalSpaceItemDecoration
import kotlinx.android.synthetic.main.list_fragment.*
import org.koin.android.viewmodel.ext.android.viewModel

class PrefFragment : Fragment() {

    val vModel : PrefViewModel by viewModel()
    val data = ArrayList<RedditImage>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        (activity as AppCompatActivity).supportActionBar?.title = getString(R.string.preferiti)
        return inflater.inflate(R.layout.list_fragment,container,false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        input_layout.isVisible = false
        val adapter = ImagesGridAdapter(data){
                index,view ->
                val action = PrefFragmentDirections.actionPrefFragmentToFragmentPager(data.toTypedArray(),index,true)
            findNavController().navigate(action)
        }
        picture_list.layoutManager = GridLayoutManager(requireContext(),3)
        picture_list.addItemDecoration(VerticalSpaceItemDecoration(24))
        picture_list.adapter = adapter
        vModel.images.observe(viewLifecycleOwner){
            data.clear()
            if(it.size >0){
                data.addAll(it)
                empty.isVisible = false
            }
            else{
                empty.isVisible = true
            }

            adapter.notifyDataSetChanged()
        }
    }
}