package com.suappstudio.iquiitest.pref

import android.app.Notification
import android.content.Context
import com.suappstudio.iquiitest.AppDatabase
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.coroutines.flow.Flow

class PrefRepo(val context: Context) {

    fun getImages() : Flow<List<RedditImage>> {
        return AppDatabase.getInstance(context).imagesDao.getImagesFlow()
    }

}