package com.suappstudio.iquiitest.pref

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.liveData
import com.suappstudio.iquiitest.model.RedditImage
import kotlinx.coroutines.flow.collect

class PrefViewModel(val app : Application, val prefRepo : PrefRepo) : AndroidViewModel(app) {

    val images = liveData<List<RedditImage>> {
       prefRepo.getImages().collect {
           emit(it)
       }
    }
}