package com.suappstudio.iquiitest.repo

import com.indra.lyc.network.BaseRepository
import com.indra.lyc.network.ResultWrapper
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import com.suappstudio.iquiitest.model.RedditImage
import com.suappstudio.iquiitest.model.RedditResponse
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RedditeRepo : BaseRepository() {

        val service = Retrofit.Builder()
             .baseUrl(BASE_URL)
            .client(OkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build().create(RedditApi::class.java)


    suspend fun getImages(word : String) : ResultWrapper<Response<RedditResponse>>{

       return safeApiCall {
            service.getImages(word)
        }



    }



}