package com.suappstudio.iquiitest.repo

import com.suappstudio.iquiitest.model.RedditResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

const val BASE_URL="https://www.reddit.com/r/"

interface RedditApi {

    @GET("{word}/top.json")
   suspend fun getImages(@Path("word") word : String) : Response<RedditResponse>

}